import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import '@/assets/scss/aui.scss'
import '@/router/routerIntercept';
import './registerServiceWorker';
import axios from 'axios'
import router from "./router/router";

const app = createApp(App)
app.config.globalProperties.$axios = axios
app.use(ElementPlus)
app.use(store)
app.use(router)

router.isReady().then(() => {
  app.mount('#app')
})
