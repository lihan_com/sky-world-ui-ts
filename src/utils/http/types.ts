import type { AxiosRequestConfig } from 'axios'
import { AxiosTransform } from './axiosTransform'

// 请求基础参数
export interface RequestOptions {
  // 是否加入url 前缀
  joinPrefix?: boolean;
  // 请求参数是否拼接到URL
  splitJoinToUrl: boolean;
  // 格式化时间
  formatterDate: boolean;
  // 接口地址
  apiUrl: string;
  // 错误消息提示类型
  errorMessageMode?: 'none' | 'modal';
  //  是否处理请求结果
  isTransformRequestResult?: boolean;
}
export interface CreateAxiosOptions extends AxiosRequestConfig {
  prefixUrl?: string;
  // 时间转换
  transForm?: AxiosTransform;
  requestOptions?: RequestOptions;
}

export interface Result<T = any> {
  code: number;
  success: boolean;
  msg: string;
  data: T;
}
