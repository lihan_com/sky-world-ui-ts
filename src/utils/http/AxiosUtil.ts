import type { AxiosRequestConfig, AxiosInstance, AxiosResponse } from 'axios'

import axios from 'axios'
import { AxiosCanceler } from './axiosCancel'
import { isFunction } from '@/utils/is'
import { cloneDeep } from 'lodash-es'

import type { RequestOptions, CreateAxiosOptions, Result } from './types'
import { errorResult } from '@/utils/http/const'
export * from './axiosTransform'

export class MyAxios {
  private axiosInstance: AxiosInstance;
  private options: CreateAxiosOptions;

  /**
   * 构造函数
   */
  constructor (options: CreateAxiosOptions) {
    this.options = options
    this.axiosInstance = axios.create(options)
    this.setInterceptor()
  }

  /**
   * 创建axios实例
   */
  private createAxios (config: CreateAxiosOptions): void {
    this.axiosInstance = axios.create(config)
  }

  /**
   * 重新配置axios
   */
  resetConfigAxios (config: CreateAxiosOptions) {
    if (!this.axiosInstance) {
      return
    }
    this.createAxios(config)
  }

  private getTransForm () {
    const { transForm } = this.options
    return transForm
  }

  getAxios (): AxiosInstance {
    return this.axiosInstance
  }

  /**
   * 设置通用header
   */
  setHeader (headers: any): void {
    if (!this.axiosInstance) {
      return
    }
    Object.assign(this.axiosInstance.defaults.headers, headers)
  }

  /**
   * 拦截器配置
   */
  private setInterceptor () {
    const transform = this.getTransForm()

    if (!transform) {
      return
    }

    const {
      requestInterceptors,
      requestInterceptorsCatch,
      responseInterceptors,
      responseInterceptorsCatch
    } = transform

    const axiosCanceler = new AxiosCanceler()

    // 请求拦截器配置处理
    this.axiosInstance.interceptors.request.use((config: AxiosRequestConfig) => {
      const { headers: { ignoreCancelToken } = { ignoreCancelToken: false } } = config
      !ignoreCancelToken && axiosCanceler.addPending(config)
      if (requestInterceptors && isFunction(requestInterceptors)) {
        config = requestInterceptors(config)
      }
      return config
    }, undefined)

    // 请求拦截器错误捕获
    requestInterceptorsCatch &&
    isFunction(requestInterceptorsCatch) &&
    this.axiosInstance.interceptors.request.use(undefined, requestInterceptorsCatch)

    // 响应结果拦截器处理
    this.axiosInstance.interceptors.response.use((res: AxiosResponse<any>) => {
      res && axiosCanceler.removePending(res.config)
      if (responseInterceptors && isFunction(responseInterceptors)) {
        res = responseInterceptors(res)
      }
      return res
    }, undefined)

    // 响应结果拦截器错误捕获
    responseInterceptorsCatch &&
    isFunction(responseInterceptorsCatch) &&
    this.axiosInstance.interceptors.response.use(undefined, responseInterceptorsCatch)
  }

  /**
   * @description:   请求方法
   */
  request<T = any> (config: AxiosRequestConfig, options?: { errorMessageMode: string }): Promise<T> {
    let conf: AxiosRequestConfig = cloneDeep(config)
    const transform = this.getTransForm()
    const { requestOptions } = this.options
    const opt: RequestOptions = Object.assign({}, requestOptions, options)
    const { beforeRequestHook, requestCatch, transformRequestData } = transform || {}
    if (beforeRequestHook && isFunction(beforeRequestHook)) {
      conf = beforeRequestHook(conf, opt)
    }
    return new Promise((resolve, reject) => {
      this.axiosInstance
        .request<any, AxiosResponse<Result>>(conf)
        .then((res: AxiosResponse<Result>) => {
          if (transformRequestData && isFunction(transformRequestData)) {
            const ret = transformRequestData(res, opt)
            ret !== errorResult ? resolve(ret) : reject(new Error('request error!'))
            return
          }
          const data = res.data.data
          resolve((data as unknown) as Promise<T>)
        })
        .catch((e: Error) => {
          if (requestCatch && isFunction(requestCatch)) {
            reject(requestCatch(e))
            return
          }
          reject(e)
        })
    })
  }
}
