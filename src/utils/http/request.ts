import type { AxiosResponse } from 'axios'
import type { RequestOptions, CreateAxiosOptions, Result } from './types'
import { MyAxios } from '@/utils/http/AxiosUtil'
import { Method, ContentType } from '@/constant/enums/HttpEnum'
import { AxiosTransform } from './axiosTransform'
import { deepMerge, setParamsToUrl } from '@/utils/common'
import { urlPreFix } from '@/config/env'
import { isString } from '@/utils/is'
import { formatRequestDate } from '@/utils/DateUtil'
import { getToken } from '@/utils/token/TokenUtil'

const prefix = urlPreFix
/**
 * 请求拦截器
 * (1)判断是否登录
 */
const transForm: AxiosTransform = {
  /**
   * 处理请求数据
   */
  transformRequestData: (res: AxiosResponse<Result>, options: RequestOptions) => {
    const { isTransformRequestResult } = options
    // 不进行任何处理，直接返回
    // 用于页面代码可能需要直接获取code，data，message这些信息时开启
    if (!isTransformRequestResult || res.data.success) {
      return res.data.data
    }
  },
  // 请求之前处理config
  beforeRequestHook: (config, options) => {
    const {
      apiUrl,
      joinPrefix,
      splitJoinToUrl,
      formatterDate
    } = options
    if (joinPrefix) {
      config.url = `${prefix}${config.url}`
    }

    if (apiUrl && isString(apiUrl)) {
      config.url = `${apiUrl}${config.url}`
    }

    if (isString(config.params)) {
      // 兼容restful风格
      config.url = config.url + config.params
      config.params = undefined
    } else {
      if (config.method === Method.GET) {
        const now = new Date().getTime()
        config.data = {
          params: Object.assign(config.params || {}, {
            _t: now
          })
        }
      } else {
        formatterDate && formatRequestDate(config.params)
        config.data = config.params
        config.params = undefined
        if (splitJoinToUrl) {
          config.url = setParamsToUrl(config.url as string, config.data)
        }
      }
    }
    return config
  },

  /**
   * @description: 请求拦截器处理，判断是否已经登录
   */
  requestInterceptors: (config) => {
    // 请求之前处理config
    const token = getToken()
    if (token) {
      // jwt token
      config.headers.Authorization = token
    }
    return config
  }
}

function initAxios (opt?: Partial<CreateAxiosOptions>) {
  return new MyAxios(
    deepMerge({
      timeout: 10 * 1000,
      prefixUrl: prefix,
      transForm,
      headers: { 'Content-Type': ContentType.JSON },
      requestOptions: {
        // 默认将prefix 添加到url
        joinPrefix: true,
        // 需要对返回数据进行处理
        isTransformRequestResult: true,
        // post请求的时候添加参数到url
        joinParamsToUrl: false,
        // 格式化提交参数时间
        formatDate: true,
        // 消息提示类型
        errorMessageMode: 'none'
      }
    }, opt || {})
  )
}

export const httpClient = initAxios()
