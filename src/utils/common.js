export function deepMerge(src, target) {
    let key;
    for (key in target) {
        src[key] =
            src[key] && src[key].toString() === '[object Object]'
                ? deepMerge(src[key], target[key])
                : (src[key] = target[key]);
    }
    return src;
}
/**
 *  设置参数到地址后面，用于get请求
 * @param baseUrl
 * @param obj
 */
export function setParamsToUrl(baseUrl, obj) {
    let parameters = '';
    let url = '';
    for (const key in obj) {
        parameters += key + '=' + encodeURIComponent(obj[key]) + '&';
    }
    parameters = parameters.replace(/&$/, '');
    if (/\?$/.test(baseUrl)) {
        url = baseUrl + parameters;
    }
    else {
        url = baseUrl.replace(/\/?$/, '?') + parameters;
    }
    return url;
}
//# sourceMappingURL=common.js.map