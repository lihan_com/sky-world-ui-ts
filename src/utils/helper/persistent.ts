import { BASE_LOCAL_CACHE_KEY, BASE_SESSION_CACHE_KEY } from '@/constant/enums/CacheEnum'

interface CacheStore {
  local?: any;
  session?: any;
}

const cacheStore: CacheStore = {
  local: {},
  session: {}
}

export function setSession (key: string, value: any) {
  cacheStore.session[BASE_SESSION_CACHE_KEY] = cacheStore.session[BASE_SESSION_CACHE_KEY] || {}
  cacheStore.session[BASE_SESSION_CACHE_KEY][key] = value
}

export function getSession<T> (key: string): T | null {
  try {
    return cacheStore.session[BASE_SESSION_CACHE_KEY][key]
  } catch (error) {
    return null
  }
}

export function getLocal<T> (key: string): T | null {
  try {
    return cacheStore.local[BASE_LOCAL_CACHE_KEY][key]
  } catch (error) {
    return null
  }
}
