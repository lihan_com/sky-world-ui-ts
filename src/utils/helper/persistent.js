import { BASE_LOCAL_CACHE_KEY, BASE_SESSION_CACHE_KEY } from '@/constant/enums/CacheEnum';
const cacheStore = {
    local: {},
    session: {}
};
export function setSession(key, value) {
    cacheStore.session[BASE_SESSION_CACHE_KEY] = cacheStore.session[BASE_SESSION_CACHE_KEY] || {};
    cacheStore.session[BASE_SESSION_CACHE_KEY][key] = value;
}
export function getSession(key) {
    try {
        return cacheStore.session[BASE_SESSION_CACHE_KEY][key];
    }
    catch (error) {
        return null;
    }
}
export function getLocal(key) {
    try {
        return cacheStore.local[BASE_LOCAL_CACHE_KEY][key];
    }
    catch (error) {
        return null;
    }
}
//# sourceMappingURL=persistent.js.map