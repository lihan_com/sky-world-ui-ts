import type { VNode } from 'vue'
import type { ModalOptions } from 'ant-design-vue/types/modal'

export type MessageType = 'success' | 'warning' | 'info' | 'error';

export interface CloseEventHandler {
  (instance: MessageComponent): void;
}

export declare class MessageComponent {
  close(): void;
}
export interface ModalOptionsEx extends Omit<ModalOptions, 'iconType'> {
  iconType: 'warning' | 'success' | 'error' | 'info';
}
export type ModalOptionsPartial = Partial<ModalOptionsEx> & Pick<ModalOptionsEx, 'content'>;
