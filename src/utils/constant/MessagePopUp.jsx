import { Modal, message as Message } from 'ant-design-vue';
const baseOptions = {
    okText: '确定',
    centered: true
};
function renderContent({ content }) {
    return <div innerHTML={`<div>${content}</div>`}></div>;
}
function getIcon(iconType) {
    if (iconType === 'warning') {
        return '<InfoCircleFilled class="modal-icon-warning" />';
    }
    else if (iconType === 'success') {
        return '<CheckCircleFilled class="modal-icon-success" />';
    }
    else if (iconType === 'info') {
        return '<InfoCircleFilled class="modal-icon-info" />';
    }
    else {
        return '<CloseCircleFilled class="modal-icon-error" />';
    }
}
function createModalOptions(options, icon) {
    return {
        ...baseOptions,
        ...options,
        content: renderContent(options),
        icon: getIcon(icon)
    };
}
function createSuccessModal(options) {
    return Modal.success(createModalOptions(options, 'success'));
}
function createErrorModal(options) {
    return Modal.error(createModalOptions(options, 'close'));
}
function createInfoModal(options) {
    return Modal.info(createModalOptions(options, 'info'));
}
function createWarningModal(options) {
    return Modal.warning(createModalOptions(options, 'warning'));
}
export function useMessage() {
    return {
        createMessage: Message,
        createSuccessModal,
        createErrorModal,
        createInfoModal,
        createWarningModal
    };
}
//# sourceMappingURL=MessagePopUp.jsx.map