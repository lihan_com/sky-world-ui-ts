import { ModalOptionsEx, ModalOptionsPartial } from '@/utils/constant/MessageType'
import { Modal, message as Message } from 'ant-design-vue'
import { InfoCircleFilled, CheckCircleFilled, CloseCircleFilled } from '@ant-design/icons'

const baseOptions = {
  okText: '确定',
  centered: true
}

function renderContent ({ content }: Pick<ModalOptionsEx, 'content'>) {
  return <div innerHTML={ `<div>${content}</div>` }></div>
}

function getIcon (iconType: string) {
  if (iconType === 'warning') {
    return '<InfoCircleFilled class="modal-icon-warning" />'
  } else if (iconType === 'success') {
    return '<CheckCircleFilled class="modal-icon-success" />'
  } else if (iconType === 'info') {
    return '<InfoCircleFilled class="modal-icon-info" />'
  } else {
    return '<CloseCircleFilled class="modal-icon-error" />'
  }
}

function createModalOptions (options: ModalOptionsPartial, icon: string): ModalOptionsPartial {
  return {
    ...baseOptions,
    ...options,
    content: renderContent(options),
    icon: getIcon(icon)
  }
}

function createSuccessModal (options: ModalOptionsPartial) {
  return Modal.success(createModalOptions(options, 'success'))
}
function createErrorModal (options: ModalOptionsPartial) {
  return Modal.error(createModalOptions(options, 'close'))
}
function createInfoModal (options: ModalOptionsPartial) {
  return Modal.info(createModalOptions(options, 'info'))
}
function createWarningModal (options: ModalOptionsPartial) {
  return Modal.warning(createModalOptions(options, 'warning'))
}

export function useMessage () {
  return {
    createMessage: Message,
    createSuccessModal,
    createErrorModal,
    createInfoModal,
    createWarningModal
  }
}
