import { userStore } from '@/store/modules/user'

/**
 * @description:  获取token
 */
export function getToken (): string {
  return userStore.getTokenState
}
