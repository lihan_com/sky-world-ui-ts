import { userStore } from '@/store/modules/user';
/**
 * @description:  获取token
 */
export function getToken() {
    return userStore.getTokenState;
}
//# sourceMappingURL=TokenUtil.js.map