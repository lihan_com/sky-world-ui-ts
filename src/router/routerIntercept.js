import router from './router';
router.beforeEach((to, from, next) => {
    const hasToken = localStorage.getItem('TOKEN-VUE3-TS-EL-ADMIN');
    const whiteList = ['/login'];
    if (hasToken) {
        if (to.path === '/login') {
            next({ path: '/' });
        }
        else {
            // 根据权限加载路由
            // ......
            next();
        }
    }
    else {
        if (whiteList.includes(to.path)) {
            next();
        }
        else {
            next(`/login`);
        }
    }
});
//# sourceMappingURL=routerIntercept.js.map