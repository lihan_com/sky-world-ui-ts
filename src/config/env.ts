
/**
 * 开发模式
 */
export const devMode = 'development'

/**
 * 生产模式
 */
export const prodMode = 'production'

export const urlPreFix = '/api'
