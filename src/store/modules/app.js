import { __decorate } from "tslib";
import store from '@/store';
import { hotModuleUnregisterModule } from '@/utils/helper/baseHelper';
import { VuexModule, Module, Mutation, getModule } from 'vuex-module-decorators';
import { PROJ_CFG_KEY } from '@/constant/enums/CacheEnum';
import { getLocal } from '@/utils/helper/persistent';
let timeId;
const NAME = 'app';
hotModuleUnregisterModule(NAME);
let App = class App extends VuexModule {
    constructor() {
        super(...arguments);
        this.pageLoadingState = false;
        this.projectConfigState = getLocal(PROJ_CFG_KEY);
    }
    commitPageLoadingState(loading) {
        this.pageLoadingState = loading;
    }
    get getProjectConfig() {
        return this.projectConfigState || {};
    }
};
__decorate([
    Mutation
], App.prototype, "commitPageLoadingState", null);
App = __decorate([
    Module({ dynamic: true, namespaced: true, store, name: NAME })
], App);
export { App };
export const appStore = getModule(App);
//# sourceMappingURL=app.js.map