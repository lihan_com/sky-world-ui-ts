import { __decorate } from "tslib";
import { appStore } from './app';
import { loginApi } from '@/api/user/user';
import router from '@/router/router';
import store from '@/store/index';
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import { setSession, getSession } from '@/utils/helper/persistent';
import { TOKEN_KEY } from '@/constant/enums/CacheEnum';
import { PageEnum } from '@/constant/enums/PageEnum';
const NAME = 'user';
let User = class User extends VuexModule {
    constructor() {
        super(...arguments);
        /**
         * 令牌
         */
        this.accessToken = '';
    }
    /**
     * 登录
     * @param params 请求参数
     */
    async login(params, toHome = true) {
        try {
            const data = await loginApi(params);
            const { accessToken } = data;
            localStorage.setItem('TOKEN-VUE3-TS-EL-ADMIN', accessToken);
            this.commitTokenState(accessToken);
            toHome &&
                (await router.push(PageEnum.BASE_HOME).then(() => {
                    setTimeout(() => {
                        appStore.commitPageLoadingState(false);
                    });
                }));
        }
        catch (error) {
            return null;
        }
    }
    commitTokenState(accessToken) {
        this.accessToken = accessToken;
        if (accessToken) {
            setSession(TOKEN_KEY, accessToken);
        }
    }
    get getTokenState() {
        return this.accessToken || getSession(TOKEN_KEY);
    }
};
__decorate([
    Action
], User.prototype, "login", null);
__decorate([
    Mutation
], User.prototype, "commitTokenState", null);
User = __decorate([
    Module({ namespaced: true, name: NAME, dynamic: true, store })
], User);
export { User };
export const userStore = getModule(User);
//# sourceMappingURL=user.js.map