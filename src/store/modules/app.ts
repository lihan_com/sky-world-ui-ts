
import store from '@/store'
import { hotModuleUnregisterModule } from '@/utils/helper/baseHelper'
import { VuexModule, Module, Mutation, getModule } from 'vuex-module-decorators'
import { ProjectConfig } from '@/types/config'
import { PROJ_CFG_KEY, LOCK_INFO_KEY } from '@/constant/enums/CacheEnum'
import { getLocal } from '@/utils/helper/persistent'

let timeId: ReturnType<typeof setTimeout>
const NAME = 'app'
hotModuleUnregisterModule(NAME)
@Module({ dynamic: true, namespaced: true, store, name: NAME })
class App extends VuexModule {
  private pageLoadingState = false
  private projectConfigState: ProjectConfig | null = getLocal(PROJ_CFG_KEY);

  @Mutation
  commitPageLoadingState (loading: boolean): void {
    this.pageLoadingState = loading
  }

  get getProjectConfig (): ProjectConfig {
    return this.projectConfigState || ({} as ProjectConfig)
  }
}
export { App }
export const appStore = getModule<App>(App)
