import type {
  LoginPO,
  AuthEntity
} from '@/entity/user/UserEntity'

import { appStore } from './app'

import { loginApi } from '@/api/user/user'
import router from '@/router/router'
import store from '@/store/index'
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators'
import { setSession, getSession } from '@/utils/helper/persistent'
import { TOKEN_KEY } from '@/constant/enums/CacheEnum'
import { PageEnum } from '@/constant/enums/PageEnum'

const NAME = 'user'
@Module({ namespaced: true, name: NAME, dynamic: true, store })
class User extends VuexModule {
  /**
   * 令牌
   */
  private accessToken = ''

  /**
   * 登录
   * @param params 请求参数
   */
  @Action
  async login (params: LoginPO, toHome = true) {
    try {
      const data = await loginApi(params)
      const { accessToken } = data
      localStorage.setItem('TOKEN-VUE3-TS-EL-ADMIN', accessToken)
      this.commitTokenState(accessToken)
      toHome &&
    (await router.push(PageEnum.BASE_HOME).then(() => {
      setTimeout(() => {
        appStore.commitPageLoadingState(false)
      })
    }))
    } catch (error) {
      return null
    }
  }

  @Mutation
  commitTokenState (accessToken: string): void {
    this.accessToken = accessToken
    if (accessToken) {
      setSession(TOKEN_KEY, accessToken)
    }
  }

  get getTokenState (): string {
    return this.accessToken || (getSession(TOKEN_KEY) as string)
  }
}

export { User }
export const userStore = getModule<User>(User)
