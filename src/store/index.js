import { createStore } from 'vuex';
import { config } from 'vuex-module-decorators';
config.rawError = true;
const store = createStore({
    modules: {}
});
export function setupStore(app) {
    app.use(store);
}
export default store;
//# sourceMappingURL=index.js.map