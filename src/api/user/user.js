import { httpClient } from '@/utils/http/request';
import { Method } from '@/constant/enums/HttpEnum';
var API;
(function (API) {
    API["Login"] = "/sky-auth/token";
})(API || (API = {}));
export function loginApi(params) {
    return httpClient.request({
        url: API.Login,
        method: Method.GET,
        params
    }, {
        errorMessageMode: 'modal'
    });
}
//# sourceMappingURL=user.js.map