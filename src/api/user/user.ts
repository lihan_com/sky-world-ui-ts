
import { httpClient } from '@/utils/http/request'

import type {
  LoginPO,
  AuthEntity
} from '@/entity/user/UserEntity'

import {
  Method
} from '@/constant/enums/HttpEnum'

enum API {
  Login = '/sky-auth/token',
}

export function loginApi (params: LoginPO) {
  return httpClient.request<AuthEntity>(
    {
      url: API.Login,
      method: Method.GET,
      params
    }
    ,
    {
      errorMessageMode: 'modal'
    }
  )
}
