
export interface LoginPO {
  tenantNo: string;
  grantType: string;
  refreshToken: string;
  account: string;
  password: string;
}

export interface AuthEntity {
  accessToken: string;
  tokenType: string;
  userId: bigint;
  tenantNo: string;
  refreshToken: string;
  oauthId: string;
  expiresIn: string;
}
