export var PageEnum;
(function (PageEnum) {
    /**
     * 登录页面
     */
    PageEnum["BASE_LOGIN"] = "/login";
    /**
     * 首页
     */
    PageEnum["BASE_HOME"] = "/home";
})(PageEnum || (PageEnum = {}));
//# sourceMappingURL=PageEnum.js.map