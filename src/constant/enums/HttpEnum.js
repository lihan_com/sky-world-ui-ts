// 请求方式
export var Method;
(function (Method) {
    Method["GET"] = "GET";
    Method["POST"] = "POST";
})(Method || (Method = {}));
// 请求数据类型
export var ContentType;
(function (ContentType) {
    ContentType["JSON"] = "application/json;charset=UTF-8";
    ContentType["FORM_URLENCODED"] = "application/x-www-form-urlencoded;charset=UTF-8";
    ContentType["FORM_DATA"] = "multipart/form-data;charset=UTF-8";
})(ContentType || (ContentType = {}));
// 响应错误编码
export var ResultCodeEnum;
(function (ResultCodeEnum) {
    ResultCodeEnum[ResultCodeEnum["TIMEOUT"] = 408] = "TIMEOUT";
})(ResultCodeEnum || (ResultCodeEnum = {}));
//# sourceMappingURL=HttpEnum.js.map