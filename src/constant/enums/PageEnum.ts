export enum PageEnum {
  /**
   * 登录页面
   */
  BASE_LOGIN = '/login',
  /**
   * 首页
   */
  BASE_HOME = '/home',
}
