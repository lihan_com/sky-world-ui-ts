// 请求方式
export enum Method {
  GET = 'GET',
  POST = 'POST'
}

// 请求数据类型
export enum ContentType {
  JSON = 'application/json;charset=UTF-8',
  FORM_URLENCODED = 'application/x-www-form-urlencoded;charset=UTF-8',
  FORM_DATA = 'multipart/form-data;charset=UTF-8'
}

// 响应错误编码
export enum ResultCodeEnum{
  TIMEOUT = 408
}
