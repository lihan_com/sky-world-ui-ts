``` 
├── src                        // 源代码
 ├── api                    // 所有请求
 ├── assets                 // 主题 字体等静态资源
 ├── components             // 全局公用组件
 ├── router                 // 路由
 ├── store                  // 全局 store管理
 ├── utils                  // 全局公用方法
 ├── views                   // view
 ├── App.vue                // 入口页面
 ├── main.js                // 入口 加载组件 初始化等
``` 


**一、vue 学习**
1、在VUE项目中使用SCSS ，对SCSS的理解和使用（简单明了）

`https://blog.csdn.net/zhouzuoluo/article/details/81010331`

2、vue扩展组件文档

`https://www.iviewui.com/docs/guide/start`
`https://pro.iviewui.com/pro/component/login?ref=iview`

3、**If github.com is not accessible in your location try setting a proxy via HTTP_PROXY**

`npm install -g node-gyp`

4、vue3 兼容 element-ui

    https://element-plus.gitee.io/#/zh-CN/component/quickstart

5、vue3.0 配置dev

    https://www.cnblogs.com/hemei1212/p/12195686.html

    npm  install @vue/composition-api --save

6、vue3.0学习地址

(1)`http://www.dreamwu.com/post-1563.html`

(2)`https://www.yuque.com/gdnnth/vue-v3/getxfm`

(3)如何搭建一个完整的Vue3.0+ts的项目步骤

`npm install axios --save`

`https://www.jb51.net/article/197612.htm`
(4) 安装webpack到package.json文件中
`npm install webpack --save-dev`

(5)用yarn install 安装模块报错解决方法

`npm install -g yarn`

`https://blog.csdn.net/weixin_42221416/article/details/85788783`

(6) npm 重新构建node_modules

`npm cache clean --force`

`npm install`

(7)axios 封装

`https://www.jianshu.com/p/44500385abdd`

(8)安装ant-design-vue

`npm add ant-design-vue`

(7)vuex 使用详情

`https://blog.csdn.net/sllailcp/article/details/106402583/`

`npm install  vuex-module-decorators`

(8)
(0)
(10)
(11)

