module.exports = {
  devServer: {
    overlay: {
      warning: false,
      errors: false
    },
    host: 'localhost',
    port: 9001,
    open: true,
    proxy: {
      '/api': {
        target: 'http://localhost:80',
        // target: 'http://lihan.free.idcfengye.com',
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
}
